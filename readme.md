# Home Assistent Integration - Gardena
This code is an copy and improvement from [grm](https://github.com/grm) version of an gardena component.
He created the python liberay and created the basic functions. Unfortunately, his design is not running out of the box.
In this repo I made the corresponding changes, so that you can easily integrate it into your own smart home.

## Supported Gardena Devices and Tests
The code supports the following devices, but unfortunately i do not have all the devices to test this.

- gardena smart mower (as vacuum / not tested)
- gardena smart sensor (as sensor / tested)
- gardena smart irrigation control (as switch / tested)
- gardena smart water control (as switch / not tested)
- gardena smart power socket (as switch / not tested)

## Installation
```
cd <path>/<to>/<your>/<config>
mkdir custom_components (if not exist)
cd custom_components
git clone https://gitlab.com/tobiaskuntzsch/home-assistant-gardena-integration.git
mv home-assistant-gardena-integration/ gardena
```
## Configuration
`<path>/<to>/<your>/<config>/secrets.yaml`
```
gardena_user: <your-gardena-smart-account-email>
gardena_password: <your-gardena-smart-account-password>
```
`<path>/<to>/<your>/<config>/configuration.yml`
```
gardena:
  email: !secret gardena_user
  password: !secret gardena_password
  client_id:  <your-developer-1689-cloud-client-id>

```

### How to get your client_id
1. Go to https://developer.1689.cloud/
2. Sign in with your gardena account credentials
3. After login, you will automatic redirect to "my applications". (https://developer.1689.cloud/apps)
4. Create an new app, name it `smarthome` and leave the other fields empty
5. Click on "Connect new Api" and connect all gardena apis (Authentication API, GARDENA smart system API,Automower Connect API)
6. Copy your `Application key`, this is your new `client_id`


### Optional Config Parameter
```
location_id: <id>
```
The api currently only supports one location / gardena gateway, which is why it is used automatically. In case of problems you can  overwrite the location id.
```
smart_irrigation_duration: 30
smart_watering: 60
mower_duration: 60
```
Gardena switches are not infinitely on. This value indicates how many minutes the switch should be on.
