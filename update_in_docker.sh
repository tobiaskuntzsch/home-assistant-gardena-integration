#!/bin/sh
pod=$(kubectl get pods | grep home-assistant | awk '{print $1}')
echo $pod
kubectl cp ./__init__.py $pod:/config/custom_components/gardena/
kubectl cp ./sensor.py $pod:/config/custom_components/gardena/
kubectl cp ./switch.py $pod:/config/custom_components/gardena/
